package com.example.antoine.formavance;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.view.View.OnClickListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    final Context context = this;
    Button envoyer;
    RadioButton rd1;
    CheckBox check1;
    RadioButton rd2;
    CheckBox check2;
    CheckBox check3;
    String qualite;
    String message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        envoyer = (Button) findViewById(R.id.btnSend);
        rd1 = (RadioButton) findViewById(R.id.rdAmateur);
        rd2 = (RadioButton) findViewById(R.id.rdPro);
        check1 = (CheckBox) findViewById(R.id.cbJava);
        check2 = (CheckBox) findViewById(R.id.cbC);
        check3 = (CheckBox) findViewById(R.id.cbPhp);


        envoyer.setOnClickListener(envoyerListener);
    }
        private OnClickListener envoyerListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> checkArray = new ArrayList<>();
                if(rd2.isChecked() == true){
                     qualite = "Pro";
                }
                else if(rd1.isChecked() == true){
                     qualite = "Amateur";
                }
                if(check1.isChecked() == true){
                    checkArray.add("Java");
                }
                if(check2.isChecked() == true){
                    checkArray.add("C#");
                }
                if(check3.isChecked() == true){
                    checkArray.add("PhP");
                }
                message = "Qualité : " + qualite + "\n";
                for(int i =0; i < checkArray.size(); i++) {
                    message = message + "Langage "+i+": " + checkArray.get(i).toString() + "\n";
                }
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle("Affichage des informations");
                alertDialogBuilder
                        .setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        MainActivity.this.finish();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        };
    }

