package com.example.antoine.deuxactivite;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

public class SecondActivity extends AppCompatActivity {
    String texte;
    TextView txtParam;
    Button maps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        txtParam  = (TextView) findViewById(R.id.txtParam);
        maps = (Button) findViewById(R.id.btnMaps);
        Bundle extras = getIntent().getExtras();
        if(extras == null){
            texte = null;
            txtParam.setText(texte);
        }
        else{
            texte = extras.getString("texte");
            txtParam.setText(texte);
        }

        Context context = getApplicationContext();
        CharSequence textToast = texte;
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context,textToast,duration);
        toast.show();

        maps.setOnClickListener(mapsListener);
    }

    private OnClickListener mapsListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(SecondActivity.this, MapsActivity.class);
            String keyIdentifer = null;
            startActivity(i);
            finish();

            Context context = getApplicationContext();
            CharSequence textToast = "test";
            int duration = Toast.LENGTH_LONG;

            Toast toast = Toast.makeText(context,textToast,duration);
            toast.show();
        }
    };
}
