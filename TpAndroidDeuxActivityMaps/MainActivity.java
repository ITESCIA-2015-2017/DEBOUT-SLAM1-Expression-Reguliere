package com.example.antoine.deuxactivite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.view.View.OnClickListener;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    Button envoyer;
    EditText text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        envoyer = (Button) findViewById(R.id.btnSend);
        text = (EditText) findViewById(R.id.editTxt);

        envoyer.setOnClickListener(envoyerListener);
    }

   private OnClickListener envoyerListener = new OnClickListener(){
       @Override
       public void onClick(View v) {
           Intent i = new Intent(MainActivity.this, SecondActivity.class);
           String keyIdentifer = null;
           String texte = text.getText().toString();
           i.putExtra("texte", texte);
           startActivity(i);
           finish();
       }
   };
}
