package com.example.antoine.lotterie;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity {

    Button btnRandom = null;
    TextView rdm1 = null;
    TextView rdm2 = null;
    TextView rdm3 = null;
    TextView txtRepet = null;
    EditText txtSaisie = null;
    Random r = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnRandom = (Button) findViewById(R.id.btnRadom);
        rdm1 = (TextView) findViewById(R.id.rdm1);
        rdm2 = (TextView) findViewById(R.id.rdm2);
        rdm3 = (TextView) findViewById(R.id.rdm3);
        txtRepet = (TextView) findViewById(R.id.txtRepet);
        txtSaisie = (EditText) findViewById(R.id.txtSaisie);

        btnRandom.setOnClickListener(btnRandomListener);
    }

    private View.OnClickListener btnRandomListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int max = 9;
            int min = 1;
            String valeur = txtSaisie.getText().toString();
            int random1 = r.nextInt(max - min + 1) + min;
            int random2 = r.nextInt(max - min + 1) + min;
            int random3 = r.nextInt(max - min + 1) + min;

            String chaine1 = String.valueOf(random1);
            rdm1.setText(chaine1);
            String chaine2 = String.valueOf(random2);
            rdm2.setText(chaine2);
            String chaine3 = String.valueOf(random3);
            rdm3.setText(chaine3);

            ArrayList<String> list = new ArrayList<String>();

            list.add(chaine1);
            list.add(chaine2);
            list.add(chaine3);
            int count = 0;

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).compareTo(valeur) == 0) {
                    count++;
                }
            }
            String chaineRepet = String.valueOf(count);
            txtRepet.setText(chaineRepet);
        }
    };

}
