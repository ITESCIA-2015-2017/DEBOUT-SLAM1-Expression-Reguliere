package com.example.antoine.meumeu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button myBT = (Button) findViewById(R.id.bouton);
        myBT.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                if (view.getId() == R.id.bouton){
                    Toast.makeText(getApplication().getBaseContext(), "mon message", Toast.LENGTH_SHORT).show();
                }
            }
        }
        )
        ;
    }


}
