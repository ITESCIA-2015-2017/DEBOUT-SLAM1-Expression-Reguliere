-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 07 Juillet 2016 à 12:07
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `ppe_ventigisbi`
--

-- --------------------------------------------------------

--
-- Structure de la table `acheter`
--

CREATE TABLE IF NOT EXISTS `acheter` (
  `MED_DEPOTLEGAL` varchar(15) NOT NULL,
  `COM_ID` int(11) NOT NULL,
  PRIMARY KEY (`MED_DEPOTLEGAL`,`COM_ID`),
  KEY `FK_acheter_COM_ID` (`COM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE IF NOT EXISTS `commande` (
  `COM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `COM_DATE` date DEFAULT NULL,
  `COM_MONTANTTOTAL` float DEFAULT NULL,
  `COM_REGLE` tinyint(1) DEFAULT NULL,
  `COM_QUANTITE` int(11) DEFAULT NULL,
  `COM_PRIX` float DEFAULT NULL,
  `PRA_NUM` int(11) NOT NULL,
  PRIMARY KEY (`COM_ID`),
  KEY `FK_commande_PRA_NUM` (`PRA_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `echantillonmedic`
--

CREATE TABLE IF NOT EXISTS `echantillonmedic` (
  `ECH_NUM` int(11) NOT NULL,
  `ECH_PRIXHT` float DEFAULT NULL,
  `MED_DEPOTLEGAL` varchar(15) NOT NULL,
  PRIMARY KEY (`ECH_NUM`),
  KEY `FK_echantillonmedic_MED_DEPOTLEGAL` (`MED_DEPOTLEGAL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `echantillonmedic`
--

INSERT INTO `echantillonmedic` (`ECH_NUM`, `ECH_PRIXHT`, `MED_DEPOTLEGAL`) VALUES
(1, 3.5, 'TROXT21'),
(2, 1.5, 'AMOX45'),
(3, 2, 'PHYSOI8');

-- --------------------------------------------------------

--
-- Structure de la table `medicament`
--

CREATE TABLE IF NOT EXISTS `medicament` (
  `MED_DEPOTLEGAL` varchar(15) NOT NULL,
  `MED_NOMCOMMERCIAL` varchar(30) DEFAULT NULL,
  `MED_COMPOSITION` varchar(8000) DEFAULT NULL,
  `MED_EFFET` varchar(8000) DEFAULT NULL,
  `MED_CONTREINDIC` blob,
  `MED_PRIX` float DEFAULT NULL,
  `MED_QTSTOCK` int(11) DEFAULT NULL,
  PRIMARY KEY (`MED_DEPOTLEGAL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `medicament`
--

INSERT INTO `medicament` (`MED_DEPOTLEGAL`, `MED_NOMCOMMERCIAL`, `MED_COMPOSITION`, `MED_EFFET`, `MED_CONTREINDIC`, `MED_PRIX`, `MED_QTSTOCK`) VALUES
('3MYC7', 'TRIMYCINE', 'Triamcinolone (ac', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c657267696520e0a0206c27756e2064657320636f6e7374697475616e74732c206427696e66656374696f6e73206465206c612070656175206f75206465207061726173697469736d65206e6f6e207472616974e9732c20642761636ee92e204e6520706173206170706c69717565722073757220756e6520706c6169652c206e6920736f757320756e2070616e73656d656e74206f63636c757369662e, 33, 666),
('ADIMOL9', 'ADIMOL', 'Amoxicilline + Acide clavulanique', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c6572676965206175782070e96e6963696c6c696e6573206f75206175782063e97068616c6f73706f72696e65732e, 2, 600),
('AMOPIL7', 'AMOPIL', 'Amoxicilline', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c6572676965206175782070e96e6963696c6c696e65732e20496c20646f697420ea7472652061646d696e69737472e920617665632070727564656e636520656e20636173206427616c6c6572676965206175782063e97068616c6f73706f72696e65732e, 80, 54),
('AMOX45', 'AMOXAR', 'Amoxicilline', 'Ce m', 0x4c61207072697365206465206365206de9646963616d656e7420706575742072656e64726520706f736974696673206c65732074657374732064652064e97069737461676520647520646f706167652e, 14, 369),
('AMOXIG12', 'AMOXI G', 'Amoxicilline', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c6572676965206175782070e96e6963696c6c696e65732e20496c20646f697420ea7472652061646d696e69737472e920617665632070727564656e636520656e20636173206427616c6c6572676965206175782063e97068616c6f73706f72696e65732e, 60, 40),
('APATOUX22', 'APATOUX Vitamine C', 'Tyrothricine + T', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c657267696520e0a0206c27756e2064657320636f6e7374697475616e74732c20656e20636173206465207068e96e796c63e9746f6e75726965206574206368657a206c27656e66616e74206465206d6f696e73206465203620616e732e, 85, 50),
('BACTIG10', 'BACTIGEL', 'Erythromycine', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c65726769652061757820616e746962696f746971756573206465206c612066616d696c6c6520646573206d6163726f6c69646573206f7520646573206c696e636f73616e696465732e, 95, 600),
('BACTIV13', 'BACTIVIL', 'Erythromycine', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c657267696520617578206d6163726f6c696465732028646f6e74206c6520636865662064652066696c6520657374206c27e972797468726f6d7963696e65292e, 5, 200),
('BITALV', 'BIVALIC', 'Dextropropoxyph', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c657267696520617578206de9646963616d656e74732064652063657474652066616d696c6c652c206427696e737566666973616e63652068e970617469717565206f75206427696e737566666973616e63652072e96e616c652e, 66, 555),
('CARTION6', 'CARTION', 'Acide ac', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e206361732064652074726f75626c6573206465206c6120636f6167756c6174696f6e202874656e64616e636573206175782068e96d6f72726167696573292c206427756c63e872652067617374726f64756f64e96e616c2c206d616c61646965732067726176657320647520666f69652e, 80, 50),
('CLAZER6', 'CLAZER', 'Clarithromycine', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c657267696520617578206d6163726f6c696465732028646f6e74206c6520636865662064652066696c6520657374206c27e972797468726f6d7963696e65292e, 10, 366),
('DEPRIL9', 'DEPRAMIL', 'Clomipramine', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e2063617320646520676c6175636f6d65206f752064276164e96e6f6d65206465206c612070726f73746174652c206427696e666172637475732072e963656e742c206f7520736920766f7573206176657a207265e0a77520756e207472616974656d656e742070617220494d414f20647572616e74206c657320322073656d61696e6573207072e963e964656e746573206f7520656e20636173206427616c6c65726769652061757820616e746964e970726573736575727320696d697072616d696e69717565732e, 56, 21),
('DIMIRTAM6', 'DIMIRTAM', 'Mirtazapine', 'Ce m', 0x4c612070726973652064652063652070726f647569742065737420636f6e7472652d696e64697175e96520656e20636173206465206427616c6c657267696520e0a0206c27756e2064657320636f6e7374697475616e74732e, 91, 36),
('DOLRIL7', 'DOLORIL', 'Acide ac', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c6572676965206175207061726163e974616d6f6c206f75206175782073616c6963796c617465732e, 15, 150),
('DORNOM8', 'NORMADOR', 'Doxylamine', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e2063617320646520676c6175636f6d652c206465206365727461696e732074726f75626c6573207572696e6169726573202872e974656e74696f6e207572696e6169726529206574206368657a206c27656e66616e74206465206d6f696e7320646520313520616e732e, 11, 80),
('EQUILARX6', 'EQUILAR', 'M', 'Ce m', 0x4365206de9646963616d656e74206e6520646f69742070617320ea747265207574696c6973e920656e20636173206427616c6c65726769652061752070726f647569742c20656e2063617320646520676c6175636f6d65206f752064652072e974656e74696f6e207572696e616972652e, 50, 14),
('EVILR7', 'EVEILLOR', 'Adrafinil', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c657267696520e0a0206c27756e2064657320636f6e7374697475616e74732e, 77, 55),
('INSXT5', 'INSECTIL', 'Diph', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c65726769652061757820616e746968697374616d696e69717565732e, 100, 60),
('JOVAI8', 'JOVENIL', 'Josamycine', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c657267696520617578206d6163726f6c696465732028646f6e74206c6520636865662064652066696c6520657374206c27e972797468726f6d7963696e65292e, 15, 22),
('LIDOXY23', 'LIDOXYTRACINE', 'Oxyt', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c657267696520e0a0206c27756e2064657320636f6e7374697475616e74732e20496c206e6520646f69742070617320ea747265206173736f6369e9206175782072e974696e6fef6465732e, 13, 37),
('LITHOR12', 'LITHORINE', 'Lithium', 'Ce m', 0x4365206de9646963616d656e74206e6520646f69742070617320ea747265207574696c6973e920736920766f757320ea74657320616c6c65726769717565206175206c69746869756d2e204176616e74206465207072656e647265206365207472616974656d656e742c207369676e616c657a20e0a020766f747265206de9646563696e207472616974616e7420736920766f757320736f75666672657a206427696e737566666973616e63652072e96e616c652c206f7520736920766f7573206176657a20756e2072e967696d652073616e732073656c2e, 20, 200),
('PARMOL16', 'PARMOCODEINE', 'Cod', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c657267696520e0a0206c27756e2064657320636f6e7374697475616e74732c206368657a206c27656e66616e74206465206d6f696e73206465203135204b672c20656e20636173206427696e737566666973616e63652068e970617469717565206f752072657370697261746f6972652c206427617374686d652c206465207068e96e796c63e9746f6e75726965206574206368657a206c612066656d6d652071756920616c6c616974652e, 9, 400),
('PHYSOI8', 'PHYSICOR', 'Sulbutiamine', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c657267696520e0a0206c27756e2064657320636f6e7374697475616e74732e, 8, 100),
('PIRIZ8', 'PIRIZAN', 'Pyrazinamide', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c657267696520e0a0206c27756e2064657320636f6e7374697475616e74732c206427696e737566666973616e63652072e96e616c65206f752068e9706174697175652c206427687970657275726963e96d6965206f7520646520706f727068797269652e, 7, 99),
('POMDI20', 'POMADINE', 'Bacitracine', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c65726769652061757820616e746962696f746971756573206170706c697175e973206c6f63616c656d656e742e, 4, 95),
('TROXT21', 'TROXADET', 'Parox', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c65726769652061752070726f647569742e, 3, 500),
('TXISOL22', 'TOUXISOL Vitamine C', 'Tyrothricine + Acide ascorbique (Vitamine C)', 'Ce m', 0x4365206de9646963616d656e742065737420636f6e7472652d696e64697175e920656e20636173206427616c6c657267696520e0a0206c27756e2064657320636f6e7374697475616e7473206574206368657a206c27656e66616e74206465206d6f696e73206465203620616e732e, 150, 3),
('URIEG6', 'URIREGUL', 'Fosfomycine trom', 'Ce m', 0x4c61207072697365206465206365206de9646963616d656e742065737420636f6e7472652d696e64697175e96520656e20636173206427616c6c657267696520e0a0206c27756e2064657320636f6e7374697475616e7473206574206427696e737566666973616e63652072e96e616c652e, 10, 50);

-- --------------------------------------------------------

--
-- Structure de la table `posseder`
--

CREATE TABLE IF NOT EXISTS `posseder` (
  `PRA_NUM` int(11) NOT NULL,
  `SPE_CODE` varchar(3) NOT NULL,
  PRIMARY KEY (`PRA_NUM`,`SPE_CODE`),
  KEY `FK_posseder_SPE_CODE` (`SPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `posseder`
--

INSERT INTO `posseder` (`PRA_NUM`, `SPE_CODE`) VALUES
(42, 'CAC'),
(10, 'NEP'),
(31, 'SXL');

-- --------------------------------------------------------

--
-- Structure de la table `praticien`
--

CREATE TABLE IF NOT EXISTS `praticien` (
  `PRA_NUM` int(11) NOT NULL,
  `PRA_NOM` varchar(30) DEFAULT NULL,
  `PRA_COEF_NOTORIETE` float DEFAULT NULL,
  `PRA_LOGIN` varchar(255) DEFAULT NULL,
  `PRA_MDP` varchar(255) DEFAULT NULL,
  `VIS_ID` int(11) NOT NULL,
  PRIMARY KEY (`PRA_NUM`),
  KEY `FK_praticien_VIS_ID` (`VIS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `praticien`
--

INSERT INTO `praticien` (`PRA_NUM`, `PRA_NOM`, `PRA_COEF_NOTORIETE`, `PRA_LOGIN`, `PRA_MDP`, `VIS_ID`) VALUES
(2, 'Gosselin', 307.49, 'Gosselin', 'Password1', 17),
(3, 'Delahaye', 185.79, 'Delahaye', 'Password1', 6),
(4, 'Leroux', 172.04, 'Leroux', 'Password1', 4),
(5, 'Desmoulins', 94.75, 'Desmoulins', 'Password1', 6),
(6, 'Mouel', 45.2, 'Mouel', 'Password1', 23),
(7, 'Desgranges-Lentz', 20.07, 'Desgranges', 'Password1', 15),
(8, 'Marcouiller', 396.52, 'Marcouiller', 'Password1', 3),
(10, 'Lerat', 257.79, 'Lerat', 'Password1', 11),
(14, 'Guivarch', 114.56, 'Guivarch', 'Password1', 16),
(18, 'Gaff', 213.4, 'Gaffe', 'Password1', 3),
(19, 'Guenon', 175.89, 'Guenon', 'Password1', 8),
(20, 'Pr', 151.36, 'Prevot', 'Password1', 21),
(21, 'Houchard', 436.96, 'Houchard', 'Password1', 2),
(24, 'Goussard', 40.72, 'Goussard', 'Password1', 25),
(26, 'Coste', 441.87, 'Coste', 'Password1', 5),
(28, 'Lem', 326.4, 'Lemee', 'Password1', 12),
(29, 'Martin', 506.06, 'Martin', 'Password1', 13),
(31, 'Rosenstech', 366.82, 'Rosenstech', 'Password1', 4),
(35, 'Leveneur', 7.39, 'Leveneur', 'Password1', 9),
(36, 'Mosquet', 77.1, 'Mosquet', 'Password1', 18),
(37, 'Giraudon', 92.62, 'Giraudon', 'Password1', 8),
(39, 'Maury', 13.73, 'Maury', 'Password1', 5),
(40, 'Dennel', 550.69, 'Dennel', 'Password1', 1),
(41, 'Ain', 5.59, 'Ain', 'Password1', 10),
(42, 'Chemery', 396.58, 'Chemery', 'Password1', 16),
(46, 'Riou', 193.25, 'Riou', 'Password1', 15),
(47, 'Chubilleau', 202.07, 'Chubilleau', 'Password1', 2),
(49, 'Goessens', 548.57, 'Goessens', 'Password1', 22),
(50, 'Laforge', 265.05, 'Laforge', 'Password1', 12),
(53, 'Vittorio', 356.23, 'Vittorio', 'Password1', 3),
(55, 'Plantet-Besnier', 369.94, 'Plantet', 'Password1', 20),
(56, 'Chubilleau', 290.75, 'Chubilleau', 'Password1', 1),
(57, 'Robert', 162.41, 'Robert', 'Password1', 14),
(58, 'Jean', 375.52, 'Jean', 'Password1', 1),
(60, 'Lecuirot', 239.66, 'Lecuirot', 'Password1', 1),
(63, 'Boireaux', 454.48, 'Boireaux', 'Password1', 7),
(64, 'Cendrier', 164.16, 'Cendrier', 'Password1', 10),
(67, 'Linard', 486.3, 'Linard', 'Password1', 19),
(68, 'Lozier', 48.4, 'Lozier', 'Password1', 9),
(69, 'Dech', 253.75, 'Dechatre', 'Password1', 16),
(71, 'Lem', 118.7, 'Lemenager', 'Password1', 13),
(72, 'N', 72.54, 'Nee', 'Password1', 24),
(74, 'Chauchard', 552.19, 'Chauchard', 'Password1', 1),
(76, 'Leroy', 570.67, 'Leroy', 'Password1', 21),
(77, 'Guyot', 28.85, 'Guyot', 'Password1', 4),
(78, 'Delposen', 292.01, 'Delposen', 'Password1', 20),
(80, 'Renouf', 425.24, 'Renouf', 'Password1', 15),
(81, 'Alliet-Grach', 451.31, 'Alliet', 'Password1', 16),
(82, 'Bayard', 271.71, 'Bayard', 'Password1', 15);

-- --------------------------------------------------------

--
-- Structure de la table `proposer`
--

CREATE TABLE IF NOT EXISTS `proposer` (
  `V_NUM` int(11) NOT NULL,
  `ECH_NUM` int(11) NOT NULL,
  PRIMARY KEY (`V_NUM`,`ECH_NUM`),
  KEY `FK_proposer_ECH_NUM` (`ECH_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `proposer`
--

INSERT INTO `proposer` (`V_NUM`, `ECH_NUM`) VALUES
(7, 1),
(4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `specialite`
--

CREATE TABLE IF NOT EXISTS `specialite` (
  `SPE_CODE` varchar(3) NOT NULL,
  `SPE_LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `specialite`
--

INSERT INTO `specialite` (`SPE_CODE`, `SPE_LIBELLE`) VALUES
('ACP', 'anatomie et cytologie pathologiques '),
('AMV', 'ang'),
('ARC', 'anesth'),
('BM', 'biologie m'),
('CAC', 'cardiologie et affections cardio-vasculaires '),
('CCT', 'chirurgie cardio-vasculaire et thoracique '),
('CG', 'chirurgie g'),
('CMF', 'chirurgie maxillo-faciale '),
('COM', 'canc'),
('COT', 'chirurgie orthop'),
('CPR', 'chirurgie plastique reconstructrice et esth'),
('CU', 'chirurgie urologique '),
('CV', 'chirurgie vasculaire '),
('DN', 'diab'),
('DV', 'dermatologie et v'),
('EM', 'endocrinologie et m'),
('ETD', ''),
('GEH', 'gastro-ent'),
('GMO', 'gyn'),
('GO', 'gyn'),
('HEM', 'maladies du sang (h'),
('MBS', 'm'),
('MDT', 'm'),
('MMO', 'm'),
('MN', 'm'),
('MPR', 'm'),
('MTR', 'm'),
('NEP', 'n'),
('NRC', 'neurochirurgie '),
('NRL', 'neurologie '),
('ODM', 'orthop'),
('OPH', 'ophtalmologie '),
('ORL', 'oto-rhino-laryngologie '),
('PEA', 'psychiatrie de l''enfant et de l''adolescent '),
('PME', 'p'),
('PNM', 'pneumologie '),
('PSC', 'psychiatrie '),
('RAD', 'radiologie (radiodiagnostic et imagerie m'),
('RDT', 'radioth'),
('RGM', 'reproduction et gyn'),
('RHU', 'rhumatologie '),
('STO', 'stomatologie '),
('SXL', 'sexologie '),
('TXA', 'toxicomanie et alcoologie ');

-- --------------------------------------------------------

--
-- Structure de la table `visite`
--

CREATE TABLE IF NOT EXISTS `visite` (
  `V_NUM` int(11) NOT NULL,
  `v_DATE` date DEFAULT NULL,
  `V_LIEU` varchar(8000) DEFAULT NULL,
  `V_DESCRIPTION` varchar(8000) DEFAULT NULL,
  PRIMARY KEY (`V_NUM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `visite`
--

INSERT INTO `visite` (`V_NUM`, `v_DATE`, `V_LIEU`, `V_DESCRIPTION`) VALUES
(3, '2002-04-18', 'La Rochelle', 'M'),
(4, '2003-01-22', 'Nice', 'Changement de direction, red'),
(7, '2003-02-23', 'Bordeaux', 'RAS Changement de tel  05 89 89 89 89 Rapport annuel '),
(25, '0000-00-00', 'Marseille', 'blabla');

-- --------------------------------------------------------

--
-- Structure de la table `visiter`
--

CREATE TABLE IF NOT EXISTS `visiter` (
  `PRA_NUM` int(11) NOT NULL,
  `V_NUM` int(11) NOT NULL,
  `VIS_ID` int(11) NOT NULL,
  PRIMARY KEY (`PRA_NUM`,`V_NUM`,`VIS_ID`),
  KEY `FK_Visiter_V_NUM` (`V_NUM`),
  KEY `FK_Visiter_VIS_ID` (`VIS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `visiter`
--

INSERT INTO `visiter` (`PRA_NUM`, `V_NUM`, `VIS_ID`) VALUES
(31, 3, 11),
(10, 4, 4),
(42, 7, 2),
(39, 25, 2);

-- --------------------------------------------------------

--
-- Structure de la table `visiteur`
--

CREATE TABLE IF NOT EXISTS `visiteur` (
  `VIS_ID` int(11) NOT NULL,
  `VIS_NOM` varchar(30) DEFAULT NULL,
  `VIS_ADRESSE` varchar(8000) DEFAULT NULL,
  `VIS_CP` varchar(5) DEFAULT NULL,
  `VIS_VILLE` varchar(255) DEFAULT NULL,
  `VIS_DATEeMBAUCHE` date DEFAULT NULL,
  `VIS_LOGIN` varchar(255) DEFAULT NULL,
  `VIS_MDP` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`VIS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `visiteur`
--

INSERT INTO `visiteur` (`VIS_ID`, `VIS_NOM`, `VIS_ADRESSE`, `VIS_CP`, `VIS_VILLE`, `VIS_DATEeMBAUCHE`, `VIS_LOGIN`, `VIS_MDP`) VALUES
(0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1, 'Juttard', '34 cours Jean Jaur', '8000', 'SEDAN', '1992-11-01', 'Visiteur1', 'Password1 '),
(2, 'Labour', '38 cours Berriat', '52000', 'CHAUMONT', '1992-10-01', 'Visiteur2', 'Password1 '),
(3, 'Landr', '4 av G', '59000', 'LILLE', '1992-01-01', 'Visiteur3', 'Password1 '),
(4, 'Langeard', '39 av Jean Perrot', '93000', 'BAGNOLET', '1995-11-01', 'Visiteur4', 'Password1 '),
(5, 'Lanne', '4 r Bayeux', '30000', 'NIMES', '1996-11-01', 'Visiteur5', 'Password1 '),
(6, 'Le', '4 av Beauvert', '68000', 'MULHOUSE', '1996-11-05', 'Visiteur6', 'Password1 '),
(7, 'Le', '39 r Raspail', '53000', 'LAVAL', '1992-11-04', 'Visiteur7', 'Password1 '),
(8, 'Leclercq', '11 r Quinconce', '18000', '1998/11/03', '0000-00-00', 'Password1 ', NULL),
(9, 'Lecornu', '4 bd Mar Foch', '72000', 'LA FERTE BERNARD', '1999-10-01', 'Visiteur9', 'Password1 '),
(10, 'Lecornu', '4 r Abel Servien', '25000', 'BESANCON', '1992-05-01', 'Visiteur10', 'Password1 '),
(11, 'Lejard', '4 r Anthoard', '82000', 'MONTAUBAN', '1993-10-01', 'Visiteur11', 'Password1 '),
(12, 'Lesaulnier', '47 r Thiers', '57000', 'METZ', '1994-12-01', 'Visiteur12', 'Password1 '),
(13, 'Letessier', '5 chem Capuche', '27000', 'EVREUX', '1993-10-05', 'Visiteur13', 'Password1 '),
(14, 'Loirat', 'Les Pechers cit', '45000', 'ORLEANS', '1992-11-01', 'Visiteur14', 'Password1 '),
(15, 'Maffezzoli', '5 r Chateaubriand', '2000', 'LAON', '1992-11-01', 'Visiteur15', 'Password1 '),
(16, 'Mancini', '5 r D''Agier', '48000', 'MENDE', '1992-11-01', 'Visiteur16', 'Password1 '),
(17, 'Marcouiller', '7 pl St Gilles', '91000', 'ISSY LES MOULINEAUX', '1992-11-01', 'Visiteur17', 'Password1 '),
(18, 'Michel', '5 r Gabriel P', '61000', 'FLERS', '1992-11-01', 'Visiteur18', 'Password1 '),
(19, 'Montecot', '6 r Paul Val', '17000', 'SAINTES', '1992-11-01', 'Visiteur19', 'Password1 '),
(20, 'Notini', '5 r Lieut Chabal', '60000', 'BEAUVAIS', '1992-11-01', 'Visiteur20', 'Password1 '),
(21, 'Onfroy', '5 r Sidonie Jacolin', '37000', 'TOURS', '1992-11-01', 'Visiteur21', 'Password1 '),
(22, 'Pascreau', '57 bd Mar Foch', '64000', 'PAU', '1992-11-01', 'Visiteur22', 'Password1 '),
(23, 'Pernot', '6 r Alexandre 1 de Yougoslavie', '11000', 'NARBONNE', '1992-11-01', 'Visiteur23', 'Password1 '),
(24, 'Perrier', '6 r Aubert Dubayet', '71000', 'CHALON SUR SAONE', '1992-11-01', 'Visiteur24', 'Password1 '),
(25, 'Petit', '7 r Ernest Renan', '50000', 'SAINT LO', '1992-11-01', 'Visiteur25', 'Password1 '),
(26, 'Piquery', '9 r Vaucelles', '14000', 'CAEN', '1992-11-01', 'Visiteur26', 'Password1 '),
(27, 'Quiquandon', '7 r Ernest Renan', '29000', 'QUIMPER', '1992-11-01', 'Visiteur27', 'Password1 '),
(28, 'Retailleau', '88Bis r Saumuroise', '39000', 'DOLE', '1992-11-01', 'Visiteur28', 'Password1 '),
(29, 'Retailleau', '32 bd Ayrault', '23000', 'MONTLUCON', '1992-11-01', 'Visiteur29', 'Password1 '),
(30, 'Souron', '7B r Gay Lussac', '21000', 'DIJON', '1992-11-01', 'Visiteur30', 'Password1 '),
(31, 'Tiphagne', '7B r Gay Lussac', '62000', 'ARRAS', '1992-11-01', 'Visiteur31', 'Password1 '),
(32, 'Tr', '7D chem Barral', '12000', 'RODEZ', '1992-11-01', 'Visiteur32', 'Password1 '),
(33, 'Tusseau', '63 r Bon Repos', '28000', 'CHARTRES', '1992-11-01', 'Visiteur33', 'Password1 ');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `acheter`
--
ALTER TABLE `acheter`
  ADD CONSTRAINT `FK_acheter_COM_ID` FOREIGN KEY (`COM_ID`) REFERENCES `commande` (`COM_ID`),
  ADD CONSTRAINT `FK_acheter_MED_DEPOTLEGAL` FOREIGN KEY (`MED_DEPOTLEGAL`) REFERENCES `medicament` (`MED_DEPOTLEGAL`);

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `FK_commande_PRA_NUM` FOREIGN KEY (`PRA_NUM`) REFERENCES `praticien` (`PRA_NUM`);

--
-- Contraintes pour la table `echantillonmedic`
--
ALTER TABLE `echantillonmedic`
  ADD CONSTRAINT `FK_echantillonmedic_MED_DEPOTLEGAL` FOREIGN KEY (`MED_DEPOTLEGAL`) REFERENCES `medicament` (`MED_DEPOTLEGAL`);

--
-- Contraintes pour la table `posseder`
--
ALTER TABLE `posseder`
  ADD CONSTRAINT `FK_posseder_SPE_CODE` FOREIGN KEY (`SPE_CODE`) REFERENCES `specialite` (`SPE_CODE`),
  ADD CONSTRAINT `FK_posseder_PRA_NUM` FOREIGN KEY (`PRA_NUM`) REFERENCES `praticien` (`PRA_NUM`);

--
-- Contraintes pour la table `praticien`
--
ALTER TABLE `praticien`
  ADD CONSTRAINT `FK_praticien_VIS_ID` FOREIGN KEY (`VIS_ID`) REFERENCES `visiteur` (`VIS_ID`);

--
-- Contraintes pour la table `proposer`
--
ALTER TABLE `proposer`
  ADD CONSTRAINT `FK_proposer_ECH_NUM` FOREIGN KEY (`ECH_NUM`) REFERENCES `echantillonmedic` (`ECH_NUM`),
  ADD CONSTRAINT `FK_proposer_V_NUM` FOREIGN KEY (`V_NUM`) REFERENCES `visite` (`V_NUM`);

--
-- Contraintes pour la table `visiter`
--
ALTER TABLE `visiter`
  ADD CONSTRAINT `FK_Visiter_VIS_ID` FOREIGN KEY (`VIS_ID`) REFERENCES `visiteur` (`VIS_ID`),
  ADD CONSTRAINT `FK_Visiter_PRA_NUM` FOREIGN KEY (`PRA_NUM`) REFERENCES `praticien` (`PRA_NUM`),
  ADD CONSTRAINT `FK_Visiter_V_NUM` FOREIGN KEY (`V_NUM`) REFERENCES `visite` (`V_NUM`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
