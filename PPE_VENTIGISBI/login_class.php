<?php
class Modeles{
	public $dbh;
	
	public function __construct($dbh){
		$this->dbh = $dbh;
	}
	
	public function login($iden, $mdp){
		$requete = $this->dbh->prepare("SELECT * FROM visiteur WHERE VIS_LOGIN=:iden AND VIS_MDP=:mdp");
		$requete->bindParam(':iden', $iden);
		$requete->bindParam(':mdp', $mdp);
		$resultat = $requete->execute();
		
		return $requete;
	}
	
	public function count($iden, $mdp){
		$requete = $this->dbh->prepare("SELECT count(*) as TOTO FROM visiteur WHERE VIS_LOGIN=:iden AND VIS_MDP=:mdp");
		$requete->bindParam(':iden', $iden);
		$requete->bindParam(':mdp', $mdp);
		$resultat = $requete->execute();
		
		$ligne = $requete->fetch(PDO::FETCH_ASSOC);
		$toto = $ligne['TOTO'];
		
		
		return $toto;
	}
	
}