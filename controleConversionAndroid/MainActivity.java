package com.example.antoine.conversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {

    Button metre = null;
    Button miles = null;
    Button kilometre = null;
    Button feet = null;
    EditText saisie = null;
    TextView resultat = null;
    EditText saisie2 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        metre = (Button) findViewById(R.id.btnMetre);
        miles = (Button) findViewById(R.id.btnMiles);
        kilometre = (Button) findViewById(R.id.btnKm);
        feet = (Button) findViewById(R.id.btnFeet);
        saisie = (EditText) findViewById(R.id.txtSaisie);
        saisie2 = (EditText) findViewById(R.id.txtSasie2);
        resultat = (TextView) findViewById(R.id.txtResult);

        metre.setOnClickListener(metreListener);
        miles.setOnClickListener(milesListener);
        kilometre.setOnClickListener(kmListener);
       feet.setOnClickListener(feetListener);
    }
       private OnClickListener metreListener = new OnClickListener() {
           @Override
           public void onClick(View v) {
               double v1 = Double.valueOf(saisie.getText().toString());
               String v2 = saisie2.getText().toString();

               switch (v2){
                   case "1" :
                       double result = v1 * 1;
                       String chaineResultat = String.valueOf(result);
                       resultat.setText(chaineResultat);
                       break;
                   case "2" :
                       result = v1 * 1609.34;
                       chaineResultat = String.valueOf(result);
                       resultat.setText(chaineResultat);
                       break;
                   case "3":
                       result = v1 * 0.001;
                       chaineResultat = String.valueOf(result);
                       resultat.setText(chaineResultat);
                       break;
                   case "4" :
                       result = v1 * 3.28084;
                       chaineResultat = String.valueOf(result);
                       resultat.setText(chaineResultat);
                       break;
               }
           }
       };

        private OnClickListener milesListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                double v1 = Double.valueOf(saisie.getText().toString());
                String v2 = saisie2.getText().toString();

                switch (v2){
                    case "1":
                        double result = v1 * 0.000621371;
                        String chaineResultat = String.valueOf(result);
                        resultat.setText(chaineResultat);
                        break;
                    case "2":
                        result = v1 * 1;
                        chaineResultat = String.valueOf(result);
                        resultat.setText(chaineResultat);
                        break;
                    case "3":
                        result = v1 * 0.621371;
                        chaineResultat = String.valueOf(result);
                        resultat.setText(chaineResultat);
                        break;
                    case "4":
                        result = v1 * 0.000189394;
                        chaineResultat = String.valueOf(result);
                        resultat.setText(chaineResultat);
                        break;
                }
            }
        };

        private OnClickListener kmListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                double v1 = Double.valueOf(saisie.getText().toString());
                String v2 = saisie2.getText().toString();

                switch (v2){
                    case "1":
                        double result = v1 * 0.001;
                        String chaineResultat = String.valueOf(result);
                        resultat.setText(chaineResultat);
                        break;
                    case "2":
                        result = v1 * 1.60934;
                        chaineResultat = String.valueOf(result);
                        resultat.setText(chaineResultat);
                        break;
                    case "3":
                        result = v1 * 1;
                        chaineResultat = String.valueOf(result);
                        resultat.setText(chaineResultat);
                        break;
                    case "4":
                        result = v1 * 3280.84;
                        chaineResultat = String.valueOf(result);
                        resultat.setText(chaineResultat);
                        break;
                }
            }
        };

    private OnClickListener feetListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            double v1 = Double.valueOf(saisie.getText().toString());
            String v2 = saisie2.getText().toString();

            switch (v2){
                case "1":
                    double result = v1 * 3.28084;
                    String chaineResultat = String.valueOf(result);
                    resultat.setText(chaineResultat);
                    break;
                case "2":
                    result = v1 * 5280;
                    chaineResultat = String.valueOf(result);
                    resultat.setText(chaineResultat);
                    break;
                case "3":
                    result = v1 * 3280.84;
                    chaineResultat = String.valueOf(result);
                    resultat.setText(chaineResultat);
                    break;
                case "4":
                    result = v1 * 1;
                    chaineResultat = String.valueOf(result);
                    resultat.setText(chaineResultat);
                    break;
            }
        }
    };






    }