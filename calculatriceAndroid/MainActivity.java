package com.example.antoine.calculatrice;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View.OnClickListener;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    Button additionner = null;
    Button soustraire = null;
    Button diviser = null;
    Button multiplier = null;
    EditText txt1 = null;
    EditText txt2 = null;
    TextView resultat = null;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        additionner = (Button) findViewById(R.id.btnPlus);
        multiplier = (Button) findViewById(R.id.btnFois);
        diviser = (Button) findViewById(R.id.btnDivision);
        soustraire = (Button) findViewById(R.id.btnMoins);
        resultat = (TextView) findViewById(R.id.txtResultat);
        txt1 = (EditText) findViewById(R.id.txt1);
        txt2 = (EditText) findViewById(R.id.txt2);

        additionner.setOnClickListener(additionnerListener);
         multiplier.setOnClickListener(multiplierListener);
        diviser.setOnClickListener(diviserListener);
        soustraire.setOnClickListener(soustraireListener);
//        txt1.setOnClickListener(txt1Listener);
//        txt2.setOnClickListener(txt2Listener);

    }

    private OnClickListener additionnerListener = new OnClickListener() {
        public void onClick(View v) {
            float f1 = Float.valueOf(txt1.getText().toString());
            float f2 = Float.valueOf(txt2.getText().toString());

            float result = f1 + f2;
            String chaineResultat = String.valueOf(result);
            resultat.setText(chaineResultat);
        }
    };

    private OnClickListener multiplierListener = new OnClickListener() {
        public void onClick(View v) {
            float f1 = Float.valueOf(txt1.getText().toString());
            float f2 = Float.valueOf(txt2.getText().toString());

            float result = f1 * f2;
            String chaineResultat = String.valueOf(result);
            resultat.setText(chaineResultat);
        }
    };

    private OnClickListener diviserListener = new OnClickListener() {
        public void onClick(View v) {
            float f1 = Float.valueOf(txt1.getText().toString());
            float f2 = Float.valueOf(txt2.getText().toString());

            float result = f1 / f2;
            String chaineResultat = String.valueOf(result);
            resultat.setText(chaineResultat);
        }
    };

    private OnClickListener soustraireListener = new OnClickListener() {
        public void onClick(View v) {
            float f1 = Float.valueOf(txt1.getText().toString());
            float f2 = Float.valueOf(txt2.getText().toString());

            float result = f1 - f2;
            String chaineResultat = String.valueOf(result);
            resultat.setText(chaineResultat);
        }
    };





}

